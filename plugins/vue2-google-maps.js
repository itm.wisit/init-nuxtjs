import Vue from 'vue'
import * as VueGoogleMaps from '~/node_modules/vue2-google-maps/src/main'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAQEGc6IXkOOOSng6qa1F8QHtTxJ5DZDps',
    libraries: 'places'
  }
})
