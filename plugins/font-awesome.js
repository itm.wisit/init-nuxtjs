import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome'

import { 
  faUser,
  faMapMarkedAlt,
  faStar
} from '@fortawesome/free-solid-svg-icons'


import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons'

library.add(
  faUser,
  faMapMarkedAlt,
  faFacebookSquare,
  faStar
)
 

Vue.component('fa', FontAwesomeIcon)
Vue.component('fa-layers', FontAwesomeLayers)