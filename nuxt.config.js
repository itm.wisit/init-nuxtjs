import settings from './settings'
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: settings.og.title || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: settings.og.description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,200;0,700;1,400&display=swap'
      }
    ]
  },
  

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#0080c6' },

  /*
   ** Global CSS
   */
  css: ['~/assets/scss/custom.scss'],

  styleResources: {
    scss: ['~/assets/vars/*.scss'],
  },


  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/font-awesome.js',
    '~/plugins/vue2-google-maps.js'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/style-resources'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',

    'nuxt-mq',

  ],
  
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: ''
  },
  
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  /** Host and port configuration */
  server: {
    port: 3000,
    host: '0.0.0.0',
  },

}
