import axios from 'axios'
import settings from '~/settings'

let axiosInstance, interceptor

/**
 *
 * @returns {axios} return axios instance
 */
const getInstance = () => {
  if (!axiosInstance) {
    axiosInstance = axios.create({
      baseURL: settings.api.baseURL
      // withCredentials: true
    })
  }
  return axiosInstance
}

/**
 *
 * @param { String | Boolean } token
 * @param { String } type
 * @param { String } scope
 */
const setToken = (token, type, scope = 'common') => {
  const value = !token ? null : (type ? type + ' ' : '') + token
  if (!value) {
    delete axiosInstance.defaults.headers[scope]['Authorization']
    return
  }
  axiosInstance.defaults.headers[scope]['Authorization'] = value
}

/**
 *
 * @param { Function } error
 * @returns { Object } response.data
 */
const setInterceptors = error => {
  // axiosInstance.interceptors.response.eject(interceptor)

  interceptor = response => {
    switch (response.status) {
      case 200:
        return response.data
        break

      default:
        return Promise.reject('error')
    }
  }

  axiosInstance.interceptors.response.use(interceptor, error =>
    Promise.reject(error)
  )
}

export { getInstance, setToken, setInterceptors }
