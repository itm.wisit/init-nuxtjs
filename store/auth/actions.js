import { AuthServices } from '~/services'
import Cookies from 'js-cookie'

export default {
  /**
   *
   * @param {String} username
   * @param {String} password
   */
  onSignIn({ commit }, { username, password, rememberMe }) {
    return new Promise(async resolve => {
      const { error, value } = await AuthServices.signIn({
        username: username,
        password: password
      })

      if (error) return resolve({ error: error })

      Cookies.set('credential', value)
      commit('UPDATE_CREDENTIAL', value)

      resolve({ value: value })
    })
  },

  onSignOff({ commit }) {
    return new Promise(resolve => {
      Cookies.remove('credential')
      commit('UPDATE_CREDENTIAL', {})
      commit('IS_LOGIN', false)
      resolve()
    })
  },

  onUpdateProfile({ commit }, value) {
    return new Promise(resolve => {
      Cookies.remove('credential')
      Cookies.set('credential', value)
      commit('UPDATE_CREDENTIAL', value)
      resolve({ value: value })
    })
  }
}
