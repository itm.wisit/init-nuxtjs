export default {
  UPDATE_CREDENTIAL: (state, value) => (state.credential = value),
  IS_LOGIN: (state, value) => (state.isLogin = value),
}
