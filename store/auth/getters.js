const getters = {
  user: state => (state.credential ? state.credential.profile : {}),
  isLogin: state => (state.credential && state.credential.profile ? true : false)
}

export default getters
