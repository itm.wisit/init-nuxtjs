import actions from './actions'
import mutations from './mutations'
import getters from './getters'

const state = () => {
  return {
    credential: {},
    isLogin: false
  }
}

const namespaced = false

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
