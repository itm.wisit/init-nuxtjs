const service = {
  signIn({ username, password }) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (username !== 'admin@admin.com' || password !== '1234')
          return resolve({ error: 'login fail wrong username or password.' })

        resolve({
          value: {
            token: '1234',
            profile: {
              name: 'admin@admin.com',
              lastName: 'example'
            }
          }
        })
      }, 500)
    })
  },

  // Facebook Connect
  getFacebookProfile(accessToken) {
    return $nuxt.$axios.get(
        `https://graph.facebook.com/v6.0/me?fields=first_name,last_name,email,name&${accessToken}`,
        {
          params: undefined
        }
      )
      .then(res => {
        return Promise.resolve(res.data)
      })
  }
}

export default service