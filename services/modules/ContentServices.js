
import recommend from '~/static/contents/recomend.json'


const service = {
  getRecommend() {
    return new Promise(resolve => {
      setTimeout(() => {
        if(recommend.response_code == 200) {
          resolve({ ...recommend.result })
        }
      }, 500)
    })
  }
}

export default service