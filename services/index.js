import AuthServices from '~/services/modules/AuthServices'
import ContentServices from '~/services/modules/ContentServices'

export {
  AuthServices,
  ContentServices
}